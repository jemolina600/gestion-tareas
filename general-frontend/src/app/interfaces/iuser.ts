
export interface IUser {
    idUser?: number;
    cedula: string;
    name: string;
    password: string;
    state: boolean;
    delete: boolean;
    dateInit: Date;
    dateUpdate: Date;
    rol: string;
}
