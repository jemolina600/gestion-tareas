export interface IProject {
  idProject?: number;
  name: string;
  description: string;
  alias: string;
  state: boolean;
  delete: boolean;
  dateInit: Date;
  dateEnd: Date;
  dateCreate: Date;
  dateUpdate: Date;
  userResponsable: [];
}
