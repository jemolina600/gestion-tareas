import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {PanelListComponent} from './panel-list/panel-list.component';

const routes: Routes = [
  {
    path: '',
    component: PanelListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
