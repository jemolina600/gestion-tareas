import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {IUser} from '../../../interfaces/iuser';
import {UserService} from '../user.service';
import { NgbModal, NgbModalOptions, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, Validators} from '@angular/forms';
import * as moment from 'moment';


@Component({
  selector: 'app-panel-list',
  templateUrl: './panel-list.component.html',
  styleUrls: ['./panel-list.component.css']
})
export class PanelListComponent implements OnInit {
  users: IUser[] = [];
  page: number | any = 1;
  pageSize: number | any = 10;
  constructor(private userS: UserService,
              private modalService: NgbModal) { }

  ngOnInit(): void {
    this.list();
    this.modalService.activeInstances.subscribe( res => {
      const desicion = this.modalService.hasOpenModals();
      if (!desicion){
        this.list();
      }
    });
  }
  private list(): void{
    this.userS.listUser().subscribe( (res: any | IUser) => {
      this.users = res;
    });
  }
  public openDetail(user: IUser | any): void {
    const modalRef = this.modalService.open(NgbdModalContentDetail);
    modalRef.componentInstance.data = user;

  }
  public openCreate(): void {
    const options: NgbModalOptions = {
      animation: true
    };
    this.modalService.open(NgbdModalContentCreate, options);
  }

}

// ---------------- Modal Detail-------------------------------------------------------

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ngbd-modal-content-detail',
  templateUrl: './modal/ModalDetail.html',
  styleUrls: ['./panel-list.component.css']
})
// tslint:disable-next-line:component-class-suffix
export class NgbdModalContentDetail implements OnInit{
  @Input() data: IUser | any;

  constructor(public activeModal: NgbActiveModal,
              private userS: UserService,
              private modalService: NgbModal) {}
  public deleteUser(id: number | any): void{
    this.userS.deleteUser(id).subscribe((res: any) => {
      alert('Se Elimino Exitosa Mente');
      this.activeModal.close('Close click');
    }, (error: any) => {
      alert('No se Logro Eliminar, Vuelva a Intentar');
      }
    );
  }
  public openUpdate(user: IUser | any): void {
    const modalRef = this.modalService.open(NgbdModalContentUpdate);
    modalRef.componentInstance.data = user;

  }
  ngOnInit(): void {

  }

}


// ---------------------- Modal Create --------------------------------
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ngbd-modal-content',
  templateUrl: './modal/ModalCreate.html',
  styleUrls: ['./panel-list.component.css']
})
// tslint:disable-next-line:component-class-suffix
export class NgbdModalContentCreate implements OnInit{
  constructor(public activeModal: NgbActiveModal,
              private fb: FormBuilder,
              private userS: UserService) {}
  userForm = this.fb.group({
    cedula: ['', Validators.required],
    name: ['', Validators.required],
    password: ['GeneralTareas2021'],
    email: ['', Validators.required ],
    state: ['', Validators.required],
    delete: [''],
    dateInit: [''],
    dateUpdate: [''],
    rol: ['', Validators.required],
  });
  state: string | any = 'Habilitado';
  input: boolean | any = true;
  ngOnInit(): void{
    const password: string | any = this.userForm.get('password');
    password.disable();
  }
  public  cambios(): void {
    if (this.input) {
      this.state = 'Habilitado';
    } else {
      this.state = 'Deshabilitado';
    }
  }
  public createUser(): void {
    const timestamp = moment(new Date()).format('YYYY-MM-DD 00:00:00');
    this.userForm.get('delete')?.setValue(false);
    this.userForm.get('dateInit')?.setValue(timestamp);
    this.userForm.get('dateUpdate')?.setValue(timestamp);
    this.userS.createUser(this.userForm.value).subscribe(
      res => {
        alert('Se Creo el usuario');
        this.activeModal.close('Close click');
      },
      error => {
        alert('Error al crear usuario, intente otra vez');
      }
    );
  }
}

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ngbd-modal-content-update',
  templateUrl: './modal/ModalUpdate.html',
  styleUrls: ['./panel-list.component.css']
})
// tslint:disable-next-line:component-class-suffix
export class NgbdModalContentUpdate implements OnInit{
  @Input() data: IUser|any = [];
  constructor(public activeModal: NgbActiveModal,
              private fb: FormBuilder,
              private userS: UserService,
              private modalService: NgbModal) {}
  userForm = this.fb.group({
    idUser: [''],
    cedula: ['', Validators.required],
    name: ['', Validators.required],
    password: ['', Validators.required],
    email: ['', Validators.required],
    state: ['', Validators.required],
    dateUpdate: [''],
    rol: ['', Validators.required],
  });
  state: string | any;
  input: boolean | any;
  ngOnInit(): void{
    this.userForm.get('cedula')?.setValue(this.data.cedula);
    this.userForm.get('email')?.setValue(this.data.email);
    this.userForm.get('rol')?.setValue(this.data.rol);
    this.userForm.get('name')?.setValue(this.data.name);
    this.userForm.get('state')?.setValue(this.data.state);
    this.input = this.data.state;
    this.cambios();
    this.userForm.get('password')?.setValue(this.data.password);
    this.userForm.get('idUser')?.setValue(this.data.idUser);
  }
  public  cambios(): void {
    if (this.input) {
      this.state = 'Habilitado';
    } else {
      this.state = 'Deshabilitado';
    }
  }
  public updateUser(): void {
    const timestamp = moment(new Date()).format('YYYY-MM-DD 00:00:00');
    this.userForm.get('dateUpdate')?.setValue(timestamp);
    this.userS.updateUser(this.userForm.value).subscribe(
      res => {
        alert('Se Actualizo el usuario');
        this.activeModal.close('Close click');
        this.modalService.dismissAll('se realizo update');
      },
      error => {
        alert('Error al actualizar usuario, intente otra vez');
      }
    );
  }

}
