import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import {
  NgbdModalContentDetail,
  PanelListComponent,
  NgbdModalContentCreate,
  NgbdModalContentUpdate
} from './panel-list/panel-list.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ReactiveFormsModule} from '@angular/forms';
@NgModule({
  declarations: [
    PanelListComponent,
    NgbdModalContentDetail,
    NgbdModalContentCreate,
    NgbdModalContentUpdate
  ],
  imports: [
    CommonModule,
    UserRoutingModule,
    NgbModule,
    ReactiveFormsModule
  ],
  exports: [PanelListComponent],
  bootstrap: [PanelListComponent]
})
export class UserModule { }
