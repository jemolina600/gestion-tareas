import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IUser} from '../../interfaces/iuser';
import {environment} from '../../../environments/environment';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private httpOptions: any;

  constructor(private http: HttpClient) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type' : 'application/json'
      }),
      observer: 'response'
    };
  }
  public listUser(): Observable<IUser[] | any>{
    return this.http.get<IUser[] | any>(`${environment.ENN_POINT}/user`)
      .pipe(map( res => {
        return res;
      }));
  }
  public getUserId(id: number): Observable<IUser | any>{
      const options = {
        params: new HttpParams().set('id', String(id))
      };
      return this.http.get<IUser>(`${environment.ENN_POINT}/user/data`, options)
        .pipe(map( res => {
          return res;
        }));
  }
  public updateUser(user: IUser): Observable<IUser | any>{
    return this.http.put<IUser | any>(`${environment.ENN_POINT}/user/update`, user)
      .pipe(map( res => {
        return res;
      }));
  }
  public createUser(user: IUser): Observable<IUser>{
    return this.http.post<IUser>(`${environment.ENN_POINT}/user/create`, user)
      .pipe(map( res => {
        return res;
      }));
  }
  public deleteUser(id: number): Observable<any>{
    const options = {
      params: new HttpParams().set('id', String(id))
    };
    return this.http.delete<any>(`${environment.ENN_POINT}/user/delete`, options)
      .pipe(map( res => {
        return res;
      }));
  }
}
