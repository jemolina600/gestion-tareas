import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {PanelBoardComponent} from './panel-board/panel-board.component';

const routes: Routes = [
  {
    path: '',
    component: PanelBoardComponent,
    children: [
      {
        path: 'user',
        loadChildren: () => import('../user/user.module')
          .then(m => m.UserModule)
      },
      {
        path: 'project',
        loadChildren: () => import('../project/project.module')
          .then(m => m.ProjectModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
