import { Component, OnInit } from '@angular/core';
import { faUsers, faProjectDiagram } from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.css']
})
export class SideBarComponent implements OnInit {

  faUsers = faUsers;
  faProjectDiagram = faProjectDiagram;

  constructor() { }

  ngOnInit(): void {
  }

}
