import { Component, OnInit } from '@angular/core';
import {NgbActiveModal, NgbCalendar, NgbDate, NgbDateParserFormatter, NgbModal, NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';
import {ProjectService} from '../project.service';
import {IProject} from '../../../interfaces/iproject';
import {FormBuilder, Validators} from '@angular/forms';
import { faCalendarAlt } from '@fortawesome/free-solid-svg-icons';
import * as moment from 'moment';
import {IUser} from '../../../interfaces/iuser';
import {UserService} from '../../user/user.service';

@Component({
  selector: 'app-panel-list',
  templateUrl: './panel-list.component.html',
  styleUrls: ['./panel-list.component.css']
})
export class PanelListComponent implements OnInit {

  projects: IProject[] = [];
  page: number | any = 1;
  pageSize: number | any = 10;
  constructor(private projectS: ProjectService,
              private modalService: NgbModal) { }

  ngOnInit(): void {
    this.list();
    this.modalService.activeInstances.subscribe( res => {
      const desicion = this.modalService.hasOpenModals();
      if (!desicion){
        this.list();
      }
    });
  }
  private list(): void {
    this.projectS.listProject().subscribe((res: any | IProject) => {
      this.projects = res;
    });
  }
  openCreate(): void{
    const options: NgbModalOptions = {
      animation: true
    };
    this.modalService.open(NgbdModalContentCreate, options);

  }
}

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ngbd-modal-content-create',
  templateUrl: './modal/ModalCreate.html',
  styleUrls: ['./panel-list.component.css']
})
// tslint:disable-next-line:component-class-suffix
export class NgbdModalContentCreate implements OnInit{
  faCalendar = faCalendarAlt;
  constructor(public activeModal: NgbActiveModal,
              private fb: FormBuilder,
              private projectS: ProjectService,
              private calendar: NgbCalendar,
              public formatter: NgbDateParserFormatter,
              private userS: UserService) {
    this.fromDate = calendar.getToday();
    this.toDate = calendar.getNext(calendar.getToday(), 'd', 10);
  }
  users: IUser | any ;
  hoveredDate: NgbDate | null = null;
  fromDate: NgbDate | null ;
  toDate: NgbDate | null ;
  projectForm = this.fb.group({
    name: ['', Validators.required],
    description: ['', Validators.required],
    alias: ['', Validators.required],
    state: ['', Validators.required ],
    delete: [false, Validators.required],
    dateInit: ['', Validators.required],
    dateEnd: ['', Validators.required],
    dateUpdate: [''],
    dateCreate: [''],
    UserResponsable: ['', Validators.required],
  });
  state: string | any = 'Habilitado';
  input: boolean | any = true;
  // ------------------ Calendario ----------------------------


  onDateSelection(date: NgbDate): void {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && date && date.after(this.fromDate)) {
      this.toDate = date;
    } else {
      this.toDate = null;
      this.fromDate = date;
    }
  }
  isHovered(date: NgbDate): any {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate): any {
    return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate): any {
    return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
  }

  validateInput(currentValue: NgbDate | null, input: string): NgbDate | null {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }

  // ------------------- Fin Calendario --------------------------
  ngOnInit(): void{
    const password: string | any = this.projectForm.get('password');
    password.disable();
    this.list();
  }
  private list(): void{
    this.userS.listUser().subscribe( (res: any | IUser) => {
      this.users = res;
    });
  }
  public  cambios(): void {
    if (this.input) {
      this.state = 'Habilitado';
    } else {
      this.state = 'Deshabilitado';
    }
  }
  public createProject(): void {
    const timestamp = moment(new Date()).format('YYYY-MM-DD 00:00:00');
    this.projectForm.get('delete')?.setValue(false);
    this.projectForm.get('dateInit')?.setValue(timestamp);
    this.projectForm.get('dateUpdate')?.setValue(timestamp);
    this.projectS.createUser(this.projectForm.value).subscribe(
      () => {
        alert('Se Creo el Projecto');
        this.activeModal.close('Close click');
      },
      () => {
        alert('Error al crear Projecto, intente otra vez');
      }
    );
  }
}
