import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProjectRoutingModule } from './project-routing.module';
import {NgbdModalContentCreate, PanelListComponent} from './panel-list/panel-list.component';
import {ReactiveFormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';


@NgModule({
  declarations: [
    PanelListComponent,
    NgbdModalContentCreate
  ],
    imports: [
        CommonModule,
        ProjectRoutingModule,
        ReactiveFormsModule,
        NgbModule,
        FontAwesomeModule
    ],
  exports: [PanelListComponent, NgbdModalContentCreate],
  bootstrap: [PanelListComponent, NgbdModalContentCreate]
})
export class ProjectModule { }
