import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IProject} from '../../interfaces/iproject';
import {environment} from '../../../environments/environment';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {
  private httpOptions: any;
  constructor(private http: HttpClient) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type' : 'application/json'
      }),
      observer: 'response'
    };
  }
  public listProject(): Observable<IProject[] | any>{
    return this.http.get<IProject[] | any>(`${environment.ENN_POINT}/project`)
      .pipe(map( res => {
        return res;
      }));
  }
  public getUserId(id: number): Observable<IProject | any>{
    const options = {
      params: new HttpParams().set('id', String(id))
    };
    return this.http.get<IProject>(`${environment.ENN_POINT}/project/dataProject`, options)
      .pipe(map( res => {
        return res;
      }));
  }
  public updateUser(project: IProject): Observable<IProject | any>{
    return this.http.put<IProject | any>(`${environment.ENN_POINT}/project/updateProject`, project)
      .pipe(map( res => {
        return res;
      }));
  }
  public createUser(project: IProject): Observable<IProject>{
    return this.http.post<IProject>(`${environment.ENN_POINT}/project/createProject`, project)
      .pipe(map( res => {
        return res;
      }));
  }
  public deleteUser(id: number): Observable<any>{
    const options = {
      params: new HttpParams().set('id', String(id))
    };
    return this.http.delete<any>(`${environment.ENN_POINT}/project/deleteProject`, options)
      .pipe(map( res => {
        return res;
      }));
  }
}
