package com.example.Gestion_Tareas.resources;

import com.example.Gestion_Tareas.dao.TaskDAO;
import com.example.Gestion_Tareas.dao.UserDAO;
import com.example.Gestion_Tareas.models.Task;
import com.example.Gestion_Tareas.models.User;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@RequestScoped
@Path("task")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TaskResources {
    
    @Inject
    TaskDAO taskDAO;

    @Inject
    UserDAO userDAO;

    @GET
    public Response getAllTask() {
        return Response.ok(taskDAO.getAllTask()).build();
    }


    @GET
    @Path("data")
    public Response getTask(@QueryParam("id") Long id) {
        Task task = taskDAO.findByIdTask(id);

        return Response.ok(task).build();
    }

    @PUT
    @Path("update")
    public Response updateTask(Task task) {
        Task getTask = taskDAO.findByIdTask(task.getIdTask());
        List<User> user  = task.getUserAsigned();
        for (int i = 0; i < user.size(); i++){
            User getUser = userDAO.findByIdUser(user.get(i).getIdUser());
            user.get(i).setCedula(getUser.getCedula());
            user.get(i).setPassword(getUser.getPassword());
            user.get(i).setName(getUser.getName());
            user.get(i).setEmail(getUser.getEmail());
            user.get(i).setState(getUser.getState());
            user.get(i).setDelete(getUser.getDelete());
            user.get(i).setDateInit(getUser.getDateInit());
            user.get(i).setDateUpdate(getUser.getDateUpdate());
            user.get(i).setRol(getUser.getRol());
        }
        task.setUserAsigned(user);
        getTask.setName(task.getName());
        getTask.setDescription(task.getDescription());
        getTask.setAlias(task.getAlias());
        getTask.setDateUpdate(task.getDateUpdate());
        getTask.setTimeDeleyed(task.getTimeDeleyed());
        getTask.setAdvPerce(task.getAdvPerce());
        getTask.setState(task.getState());
        getTask.setProjectId(task.getProjectId());
        getTask.setUserAsigned(task.getUserAsigned());
        taskDAO.updateTask(getTask);
        return Response.ok().build();
    }
    @POST
    @Path("create")
    public Response createTask(Task task) {
        List<User> user  = task.getUserAsigned();

        for (int i = 0; i < user.size(); i++){
            User getUser = userDAO.findByIdUser(user.get(i).getIdUser());
            user.get(i).setCedula(getUser.getCedula());
            user.get(i).setPassword(getUser.getPassword());
            user.get(i).setName(getUser.getName());
            user.get(i).setEmail(getUser.getEmail());
            user.get(i).setState(getUser.getState());
            user.get(i).setDelete(getUser.getDelete());
            user.get(i).setDateInit(getUser.getDateInit());
            user.get(i).setDateUpdate(getUser.getDateUpdate());
            user.get(i).setRol(getUser.getRol());
        }
        task.setUserAsigned(user);
        taskDAO.createTask(task);
        return Response.ok().build();
    }
    @DELETE
    @Path("delete")
    public Response deleteTask(@QueryParam("id") Long id) {
        Task getTask = taskDAO.findByIdTask(id);
        getTask.setDelete(true);
        taskDAO.deleteTask(getTask);
        return Response.ok().build();
    }
}
