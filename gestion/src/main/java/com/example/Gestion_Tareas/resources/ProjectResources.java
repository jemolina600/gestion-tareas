package com.example.Gestion_Tareas.resources;


import com.example.Gestion_Tareas.dao.ProjectDAO;
import com.example.Gestion_Tareas.dao.UserDAO;
import com.example.Gestion_Tareas.models.Project;
import com.example.Gestion_Tareas.models.User;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@RequestScoped
@Path("project")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ProjectResources {

    @Inject
    ProjectDAO projectDAO;

    @Inject
    UserDAO userDAO;

    @GET
    public Response getAllProject() {
        return Response.ok(projectDAO.getAllProject()).build();
    }


    @GET
    @Path("data")
    public Response getProject(@QueryParam("id") Long id) {
        Project project = projectDAO.findByIdProject(id);

        return Response.ok(project).build();
    }

    @PUT
    @Path("updateProject")
    public Response updateProject(Project project) {
        List<User> user  = project.getUserResponsable();

        for (int i = 0; i < user.size(); i++){
            User getUser = userDAO.findByIdUser(user.get(i).getIdUser());
            user.get(i).setCedula(getUser.getCedula());
            user.get(i).setPassword(getUser.getPassword());
            user.get(i).setName(getUser.getName());
            user.get(i).setEmail(getUser.getEmail());
            user.get(i).setState(getUser.getState());
            user.get(i).setDelete(getUser.getDelete());
            user.get(i).setDateInit(getUser.getDateInit());
            user.get(i).setDateUpdate(getUser.getDateUpdate());
            user.get(i).setRol(getUser.getRol());
        }
        project.setUserResponsable(user);
        Project getProject = projectDAO.findByIdProject(project.getIdProject());
        getProject.setName(project.getName());
        getProject.setDescription(project.getDescription());
        getProject.setAlias(project.getAlias());
        getProject.setDateUpdate(project.getDateUpdate());
        getProject.setState(project.getState());
        getProject.setUserResponsable(project.getUserResponsable());
        projectDAO.updateProject(getProject);
        return Response.ok().build();
    }
    @POST
    @Path("createProject")
    public Response createProject(Project project) {

        List<User> user  = project.getUserResponsable();

        for (int i = 0; i < user.size(); i++){
            User getUser = userDAO.findByIdUser(user.get(i).getIdUser());
            user.get(i).setCedula(getUser.getCedula());
            user.get(i).setPassword(getUser.getPassword());
            user.get(i).setName(getUser.getName());
            user.get(i).setEmail(getUser.getEmail());
            user.get(i).setState(getUser.getState());
            user.get(i).setDelete(getUser.getDelete());
            user.get(i).setDateInit(getUser.getDateInit());
            user.get(i).setDateUpdate(getUser.getDateUpdate());
            user.get(i).setRol(getUser.getRol());
        }
        project.setUserResponsable(user);
        projectDAO.createProject(project);
        return Response.ok().build();
    }
    @DELETE
    @Path("deleteProject")
    public Response deleteProject(@QueryParam("id") Long id) {
        Project getProject = projectDAO.findByIdProject(id);
        getProject.setDelete(true);
        projectDAO.deleteProject(getProject);
        return Response.ok().build();
    }
}
