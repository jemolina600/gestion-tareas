package com.example.Gestion_Tareas.resources;

import com.example.Gestion_Tareas.dao.UserDAO;
import com.example.Gestion_Tareas.models.User;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.List;

@RequestScoped
@Path("user")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UserResource {

    @Inject
    UserDAO userDAO;

    @GET
    public Response getAllUser() {
        List<User> user = userDAO.getAllUser();
        int count = user.size();
        for(int i = 0; i<count; i++){
            user.get(i).setPassword("**********");
        }
        return Response.ok(user).build();
    }


    @GET
    @Path("data")
    public Response getUser(@QueryParam("id") Long id) {
        User user = userDAO.findByIdUser(id);

        return Response.ok(user).build();
    }

    @PUT
    @Path("update")
    public Response updateUser(User user) {
        User getUser = userDAO.findByIdUser(user.getIdUser());
        getUser.setCedula(user.getCedula());
        getUser.setName(user.getName());
        getUser.setEmail(user.getEmail());
        getUser.setRol(user.getRol());
        getUser.setDateUpdate(user.getDateUpdate());
        getUser.setState(user.getState());
        userDAO.updateUser(getUser);
        return Response.ok().build();
    }
    @POST
    @Path("create")
    public Response createUser(User user) {
        userDAO.createUser(user);
        return Response.ok().build();
    }

}
