package com.example.Gestion_Tareas.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.security.Timestamp;
import java.util.List;
import java.util.Set;

@Entity
@Table(name="project", schema="public")
@NamedQueries({
        @NamedQuery(name="Project.findAll", query = "SELECT p FROM Project p WHERE p.delete = false")
})
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Project implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long idProject;
    @Column
    @Size(max = 50)
    private String name;
    @Column
    @Size(max = 300)
    private String description;
    @Column(unique = true)
    @Size(max = 50)
    private String alias;
    @Column
    private Boolean state;
    @Column
    private Boolean delete;
    @Column(name="date_init")
    private Timestamp dateInit;
    @Column(name="date_end")
    private Timestamp dateEnd;
    @Column(name="date_create")
    private Timestamp dateCreate;
    @Column(name="date_update")
    private Timestamp dateUpdate;
    @JoinColumn(name="id", nullable = false)
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<User> userResponsable;

}
