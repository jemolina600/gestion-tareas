package com.example.Gestion_Tareas.models;

import com.example.Gestion_Tareas.models.enumerations.Rol;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Set;

@Entity
@Table(name="user", schema="public")
@NamedQueries({
   @NamedQuery(name="User.findAll", query = "SELECT u FROM User u WHERE u.delete = false")
})
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long idUser;
    @Column(unique = true)
    @Size(max = 11)
    private String cedula;
    @Column
    @Size(max =80)
    private String name;
    @Column
    @Size(max = 80)
    private String email;
    @Column
    @Size(max = 80)
    private String password;
    @Column
    private Boolean state;
    @Column
    private Boolean delete;
    @Column(name="date_init")
    private Timestamp dateInit;
    @Column(name="date_update")
    private Timestamp dateUpdate;
    @Column
    @Enumerated(EnumType.STRING)
    private Rol rol;


}
