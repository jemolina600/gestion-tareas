package com.example.Gestion_Tareas.models.enumerations;

public enum Rol {
    Rol_Administrador,
    Rol_Lider,
    Registrado,
}
