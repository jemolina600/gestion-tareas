package com.example.Gestion_Tareas.dao;

import com.example.Gestion_Tareas.models.Project;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;


@Stateless(name = "projectDao", mappedName = "projectDao")
public class ProjectDAO {

    @PersistenceContext
    EntityManager em;

    public List<Project> getAllProject(){return em.createNamedQuery("Project.findAll",Project.class).getResultList();}

    public Project findByIdProject(Long idProject){ return em.find(Project.class, idProject);}

    public void updateProject(Project project){em.merge(project);}

    public void createProject(Project project){em.persist(project);}

    public void deleteProject(Project project){
        em.merge(project);
    }
    
}
