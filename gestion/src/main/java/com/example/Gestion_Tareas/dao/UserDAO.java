package com.example.Gestion_Tareas.dao;


import com.example.Gestion_Tareas.models.User;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless(name = "userDao", mappedName = "userDao")
public class UserDAO {

    @PersistenceContext
    EntityManager em;

    public List<User> getAllUser(){return em.createNamedQuery("User.findAll",User.class).getResultList();}

    public User findByIdUser(Long idUser){ return em.find(User.class, idUser);}

    public void updateUser(User user){em.merge(user);}

    public void createUser(User user){em.persist(user);}

    public void deleteUser(User user){
        em.merge(user);
    }


}
