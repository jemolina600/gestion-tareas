package com.example.Gestion_Tareas.dao;

import com.example.Gestion_Tareas.models.Task;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless(name="taskDao", mappedName = "taskDao")
public class TaskDAO {
    @PersistenceContext
    EntityManager em;

    public List<Task> getAllTask(){return em.createNamedQuery("Task.findAll",Task.class).getResultList();}

    public Task findByIdTask(Long idTask){ return em.find(Task.class, idTask);}

    public void updateTask(Task task){em.merge(task);}

    public void createTask(Task task){em.persist(task);}

    public void deleteTask(Task task){
        em.merge(task);
    }
}
