--
-- PostgreSQL database dump
--

-- Dumped from database version 13.3 (Debian 13.3-1.pgdg100+1)
-- Dumped by pg_dump version 13.3 (Ubuntu 13.3-1.pgdg20.10+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: project; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.project (
    name character varying(50),
    description character varying(300),
    alias character varying(50),
    state boolean,
    delete boolean,
    date_init timestamp(0) without time zone,
    date_end timestamp(0) without time zone,
    date_create timestamp(0) without time zone,
    date_update timestamp(0) without time zone,
    id integer NOT NULL,
    user_responsable integer NOT NULL
);


ALTER TABLE public.project OWNER TO postgres;

--
-- Name: task; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.task (
    name character varying(50),
    description character varying(300),
    alias character varying(50),
    state boolean,
    delete boolean,
    date_init timestamp(0) without time zone,
    date_end timestamp(0) without time zone,
    date_create timestamp(0) without time zone,
    date_update timestamp(0) without time zone,
    time_deleyedd timestamp(0) without time zone,
    id integer NOT NULL,
    project_id integer NOT NULL,
    advanced_percentage double precision,
    user_asigned integer NOT NULL
);


ALTER TABLE public.task OWNER TO postgres;

--
-- Name: user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."user" (
    name character varying(80),
    email character varying(80),
    password character varying(80),
    state boolean,
    delete boolean,
    date_init timestamp(0) without time zone,
    date_update timestamp(0) without time zone,
    cedula character varying(11) NOT NULL,
    rol character varying,
    id integer NOT NULL
);


ALTER TABLE public."user" OWNER TO postgres;

--
-- Name: TABLE "user"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public."user" IS 'tabla de usuarios';


--
-- Data for Name: project; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.project (name, description, alias, state, delete, date_init, date_end, date_create, date_update, id, user_responsable) FROM stdin;
project	vacio	po	t	f	\N	\N	\N	\N	1	1
\.


--
-- Data for Name: task; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.task (name, description, alias, state, delete, date_init, date_end, date_create, date_update, time_deleyedd, id, project_id, advanced_percentage, user_asigned) FROM stdin;
\.


--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."user" (name, email, password, state, delete, date_init, date_update, cedula, rol, id) FROM stdin;
esteban	kitotsu.cj@gmail.com	\N	t	t	2021-07-14 00:00:00	2021-07-14 00:00:00	1107546983	Registrado	4
exteban	jhojan.e.m.v.2018@gmail.com	\N	t	t	2021-07-14 00:00:00	2021-07-14 00:00:00	1107530698	Registrado	5
exteban	kjdaksj@gmail.com	\N	t	t	2021-07-14 00:00:00	2021-07-14 00:00:00	1107530678	Rol_Administrador	6
jhojan	jhojan.e.m.v.2018@gmail.com	**********	t	f	\N	\N	1107530620	Rol_Administrador	11
jhojan	jhojan.e.m.v.2018@gmail.com	**********	t	t	\N	\N	1107530694	Rol_Administrador	1
jhojan	jhojan.e.m.v.2018@gmail.com	**********	t	f	\N	\N	1107530653	Rol_Administrador	7
jhojan	jhojan.e.m.v.2018@gmail.com	**********	t	f	\N	2021-07-14 00:00:00	1107530631	Rol_Administrador	8
jhojan	jhojan.e.m.v.2018@gmail.com	**********	t	f	\N	2021-07-14 00:00:00	1107530647	Rol_Administrador	9
jhojan	jhojan.e.m.v.2018@gmail.com	**********	t	f	\N	2021-07-14 00:00:00	1107530689	Rol_Administrador	10
esteban	jhojan.e.m.v.2018@gmail.com	\N	t	f	2021-07-14 00:00:00	2021-07-14 00:00:00	1107530699	Registrado	3
jhojan	jhojan.e.m.v.2018@gmail.com	**********	t	t	\N	\N	1107530693	Rol_Administrador	2
\.


--
-- Name: project pk_project_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project
    ADD CONSTRAINT pk_project_id PRIMARY KEY (id);


--
-- Name: task pk_task_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.task
    ADD CONSTRAINT pk_task_id PRIMARY KEY (id);


--
-- Name: user pk_tbl_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT pk_tbl_id PRIMARY KEY (id);


--
-- Name: idx_user; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX idx_user ON public."user" USING btree (cedula);


--
-- Name: unq_project_alias; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX unq_project_alias ON public.project USING btree (alias);


--
-- Name: unq_task; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX unq_task ON public.task USING btree (alias);


--
-- Name: project fk_project_user; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project
    ADD CONSTRAINT fk_project_user FOREIGN KEY (user_responsable) REFERENCES public."user"(id);


--
-- Name: task fk_task_project; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.task
    ADD CONSTRAINT fk_task_project FOREIGN KEY (project_id) REFERENCES public.project(id);


--
-- Name: task fk_task_user; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.task
    ADD CONSTRAINT fk_task_user FOREIGN KEY (user_asigned) REFERENCES public."user"(id);


--
-- PostgreSQL database dump complete
--

